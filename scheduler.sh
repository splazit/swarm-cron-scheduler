#!/bin/bash
DIRECTORY=/scheduler/jobs
SERVICE_URL=http://docker-socat:2375/services

function run {
  curl http://docker-socat:2375/services | jq ".[] | select(.Spec.Name == \"$1\")"  > $DIRECTORY/$1.json
  cat $DIRECTORY/$1.json | jq  "{Name: (.Spec.Name), TaskTemplate: (.Spec.TaskTemplate)} | .TaskTemplate.ForceUpdate |= 1" > $DIRECTORY/$1-update.json
  constants=$(cat $DIRECTORY/$1.json | jq '{ID: (.ID), VERSION: (.Version.Index), SERVICENAME: (.Spec.Name) }' | jq -r "to_entries|map(\"\(.key)=\(.value|tostring)\")|.[]")
  for key in ${constants}; do
    eval ${key}
  done
  echo "Running service $SERVICENAME, version $VERSION"
  curl -XPOST "http://docker-socat:2375/services/$ID/update?version=$VERSION -d @$DIRECTORY/$1-update.json"
}

run $1
