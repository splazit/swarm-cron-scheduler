#!/bin/bash

if [ ! -z "$TIMEZONE" ]; then
  cp /usr/share/zoneinfo/$TIMEZONE /etc/localtime
  echo "$TIMEZONE" >  /etc/timezone
fi 

if [ -f /crontab ]; then
  cat /crontab >> /etc/crontabs/root
fi

crontab -l
exec "$@"
