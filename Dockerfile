FROM alpine 

RUN apk add -U curl jq bash tzdata && \
    mkdir -p /scheduler/jobs

ADD entrypoint.sh scheduler.sh /scheduler/

RUN chmod +x /scheduler/*.sh

ENTRYPOINT ["/scheduler/entrypoint.sh"]

CMD ["crond","-f"]
